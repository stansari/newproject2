import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  items: any; 
  constructor(public http: HttpClient) {
    this.items = [
      {title: 'Shahegul'},
      {title: 'Sheeba'},
      {title: 'Fouziya'},
      {title: 'Neeba'},
      {title: 'Imama'},
      {title: 'Tahir'},
      {title: 'Atefa'},
      {title: 'Zawar'},
      {title: 'Bilquis'},
      {title: 'Qaiser'}
  ]
  }

  filterItems(searchTerm){
 
    return this.items.filter((item) => {
        return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });    

}

}
